<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\BooksController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Books
Route::apiResource('books', BookController::class);
Route::apiResource('/borrowedbook', BorrowedBookController::class);
Route::apiResource('/returnedbook', ReturnedBookController::class);

//Patrons
Route::apiResource('patrons', PatronController::class);

//Categories
Route::get('/categories', [CategoryController::class, 'index']);

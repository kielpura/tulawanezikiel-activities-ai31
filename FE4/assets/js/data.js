var selectedRow = null

function onFormSubmit() {
        var formData = readFormData();
        if (selectedRow == null)
            insertNewRecord(formData);
        else
        resetForm();
}

function readFormData() {
    var formData = {};
    formData["name"] = document.getElementById("name").value;
    formData["eAdd"] = document.getElementById("eAdd").value;
    formData["dateReg"] = document.getElementById("dateReg").value;
    return formData;
}

function insertNewRecord(data) {
    var table = document.getElementById("patronList").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.name;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.eAdd;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = data.dateReg;
    cell4 = newRow.insertCell(3);
    cell4.innerHTML = `<a href="#"><i class="fas fa-user-edit"></i></a>
                        <a href="#" onClick="onDelete(this)"><i class="fas fa-user-times" style="color: red;"></i></a>`;
}

function resetForm() {
    document.getElementById("name").value = "";
    document.getElementById("eAdd").value = "";
    document.getElementById("dateReg").value = "";
    selectedRow = null;
}

function onDelete(td) {
    if (confirm('Are you sure to delete this record ?')) {
        row = td.parentElement.parentElement;
        document.getElementById("patronList").deleteRow(row.rowIndex);
        resetForm();
    }
}
var ctx = document.getElementById('myBarChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [{
            label: 'Borrowed Books',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: [0, 10, 5, 2, 20, 30, 85]
        },{
            label: 'Returned Books',
            backgroundColor: 'rgb(23, 104, 137)',
            borderColor: 'rgb(255, 99, 132)',
            data: [45, 30, 20, 2, 5, 10, 0],
        }]
    },

    // Configuration options go here
    options: {}
});
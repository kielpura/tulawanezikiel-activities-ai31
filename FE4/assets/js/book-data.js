var selectedRow = null

function onFormSubmit() {
        var formData = readFormData();
        if (selectedRow == null)
            insertNewRecord(formData);
        else
        resetForm();
}

function readFormData() {
    var formData = {};
    formData["bookName"] = document.getElementById("bookName").value;
    formData["author"] = document.getElementById("author").value;
    formData["copies"] = document.getElementById("copies").value;
    formData["category"] = document.getElementById("category").value;
    return formData;
}

function insertNewRecord(data) {
    var table = document.getElementById("bookList").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.bookName;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.author;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = data.copies;
    cell4 = newRow.insertCell(3);
    cell4.innerHTML = data.category;
    cell4 = newRow.insertCell(4);
    cell4.innerHTML =  `<a href="#"><i class="far fa-edit"></i></a>
                       <a href="#"><i class="far fa-handshake"></i></a>
                       <a href="#"><i class="fas fa-box-open"></i></a>
                       <a href="#" onClick="onDelete(this)"><i class="fas fa-minus-circle" style="color: red;"></i></a>`;
}

function resetForm() {
    document.getElementById("bookName").value = "";
    document.getElementById("author").value = "";
    document.getElementById("copies").value = "";
    document.getElementById("category").value = "";
    selectedRow = null;
}

function onDelete(td) {
    if (confirm('Are you sure to delete this record ?')) {
        row = td.parentElement.parentElement;
        document.getElementById("bookList").deleteRow(row.rowIndex);
        resetForm();
    }
}
import Vue from 'vue'
import App from './App.vue'
import 'bootstrap'
import Toasted from 'vue-toasted'

Vue.config.productionTip = false
Vue.config.productionTip = false
Vue.use(Toasted)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import "toastr/build/toastr.css";
import router from './router'
import store from "./store";
import toastr from "@/assets/js/toastr";

Vue.use(toastr);
Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: function (h) { return h(App) }
}).$mount('#app')

import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '@/components/index/Dashboard.vue'
import PatronManagement from '@/components/patrons/PatronManagement.vue'
import BookManagement from '@/components/books/BookManagement.vue'
import Settings from '@/components/settings/Settings.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        { name: 'dashboard', path: '/', component: Dashboard },
        { name: 'patrons', path: '/patrons', component: PatronManagement },
        { name: 'books', path: '/books', component: BookManagement },
        { name: 'settings', path: '/settings', component: Settings }
    ]
})


export default router

import axios from "@/assets/js/config/axiosConfig";
import toastr from 'toastr';

export default {
    namespaced: true,

    state: {
        returnedbooks: []
    },
    getters: {},
    mutations: {
        setBorrowedBook: (state, returned) => state.returned.push({ ...returned }),
        setBorrowedBooks: (state, returnedbooks) =>
            (state.returned = returnedbooks),
    },
    actions: {
        async returnedBook({ commit }, {returned, index}) {
            await axios
                .post("/returnedbook", returned)
                .then((res) => {
                    toastr.success(res.data.message, "Success");
                    commit('books/setNewCopies', {id: index, data: res.data.book.book.copies}, {root: true})
                })
                .catch((err) => {
                    if (err.response.status == 422) {
                        for (var i in err.response.data.errors) {
                            toastr.error(err.response.data.errors[i][0], "Failed");
                        }
                    }
                    else if(err.response.status == 404){
                        toastr.error(err.response.data.message, "Failed");
                    }
                });
        },
    },
};
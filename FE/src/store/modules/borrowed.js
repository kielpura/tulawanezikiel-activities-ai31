import axios from "@/assets/js/config/axiosConfig";
import toastr from "toastr";

export default {
    namespaced: true,

    state: {
        borrowedbooks: [],
    },
    getters: {
        getBorrowedBooks: (state) => state.borrowed,
    },
    mutations: {
        setBorrowedBook: (state, borrowed) => state.borrowed.push({ ...borrowed }),
        setBorrowedBooks: (state, borrowedBooks) =>
            (state.borrowed = borrowedBooks),
    },
    actions: {
        async borrowedBook({ commit }, {borrow, index}) {
            await axios
                .post("/borrowedbook", borrow)
                .then((res) => {
                    toastr.success(res.data.message, "Success");
                    commit('books/setNewCopies', {id: index, data: res.data.borrowedbook.book.copies}, {root: true})
                })
                .catch((err) => {
                    if (err.response.status == 422) {
                        for (var i in err.response.data.errors) {
                            toastr.error(err.response.data.errors[i][0], "Failed");
                        }
                    }
                });
        },
        async fetchBorrowedBooks({ commit }) {
            await axios
                .get("/borrowedbook")
                .then((res) => {
                    commit("setBorrowedBooks", res.data);
                })
                .catch((err) => {
                    console.error(err);
                });
        },
    },
};
